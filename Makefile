# SPDX-License-Identifier: GPL-3.0-or-later

CC = gcc
LEX = flex
YACC = bison
LD = gcc
INSTALL = install

CFLAGS = -pipe -std=iso9899:2018 -Wall -Wextra -Wpedantic -Wno-unused-function -g -Og
CPPFLAGS = -iquote include -D_XOPEN_SOURCE=700
YFLAGS = -Wall -Wcex
LFLAGS = -f
CFLAGS_DEBUG = -fanalyzer -fdiagnostics-color=always

DESTDIR ?= ''
PREFIX ?= /usr/local

all: setup out/bwrap-exec

.PHONY: setup
setup: out build include

out build include:
	mkdir -p $@

generated_headers = include/parse.h include/scan.h

clean:
	@echo removing output
	@$(RM) -- $(wildcard out/*)
	@echo removing build files
	@$(RM) -- $(wildcard build/*)
	@echo removing generated header
	@$(RM) -- $(generated_headers)

build/parse.c include/parse.h: src/parse.y
	$(YACC) $(YFLAGS) --defines=include/parse.h --output=build/parse.c $^

build/scan.c include/scan.h: src/scan.l
	$(LEX) $(LFLAGS) --header-file=include/scan.h --outfile=build/scan.c $^

build/scan.o: build/scan.c include/parse.h
	$(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<

build/parse.o: build/parse.c include/scan.h
	$(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<

build/utils.o: src/utils.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<

build/bwrap-exec.o: src/bwrap-exec.c include/bwrap-exec.h $(generated_headers)
	$(CC) $(CPPFLAGS) $(CFLAGS) $(CFLAGS_DEBUG) -c -o $@ $<

deps = bwrap-exec.o scan.o parse.o utils.o
OBJ = $(addprefix build/, $(deps))
out/bwrap-exec: $(OBJ)
	$(LD) -o $@ $^

.PHONY: install
install: out/bwrap-exec
	$(INSTALL) -D $< $(DESTDIR)$(PREFIX)/bin/bwrap-exec
	$(INSTALL) -Dm644 README.md $(DESTDIR)$(PREFIX)/share/doc/bwrap-exec/README.md
