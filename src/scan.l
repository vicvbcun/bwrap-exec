/* SPDX-License-Identifier: GPL-3.0-or-later */

%option noyywrap nounput noinput
%option nodefault
%option yylineno
%option reentrant
%option extra-type="struct extra"
%option bison-bridge
%option 8bit
%option debug
%option stack

%{
	#include "parse.h"
%}

%top {
	#include <stdbool.h>

	#include <sys/types.h>	// size_t

	struct extra {
		bool skip_if;
		bool end_sub;
	};
}

	/* should be all interesting of ASCII   -- */
UTF_8_TEXT	[\x01-\x7F]{-}[[:space:][:cntrl:]]|[\xC2-\xDF][\x80-\xBF]|\xE0[\xA0-\xBF][\x80-\xBF]|[\xE1-\xEC\xEE\xEF]([\x80-\xBF]{2})|\xED[\x80-\x9F][\x80-\xBF]|\xF0[\x90-\xBF]([\x80-\xBF]{2})|[\xF1-\xF3]([\x80-\xBF]{3})|\xF4[\x80-\x8F]([\x80-\xBF]{2})
	/*                                         -- everything after here should be multibyte encoded and valid (source: flex info page on UTF-8 encoded identifiers */

	/* exclude '`' and '$', else identical */
UTF_8_SUBS	[\x01-\x7F]{-}[`$[:space:][:cntrl:]]|[\xC2-\xDF][\x80-\xBF]|\xE0[\xA0-\xBF][\x80-\xBF]|[\xE1-\xEC\xEE\xEF]([\x80-\xBF]{2})|\xED[\x80-\x9F][\x80-\xBF]|\xF0[\x90-\xBF]([\x80-\xBF]{2})|[\xF1-\xF3]([\x80-\xBF]{3})|\xF4[\x80-\x8F]([\x80-\xBF]{2})

	/* exclude '`' */
UTF_8_VAR	[\x01-\x7F]{-}[`[:space:][:cntrl:]]|[\xC2-\xDF][\x80-\xBF]|\xE0[\xA0-\xBF][\x80-\xBF]|[\xE1-\xEC\xEE\xEF]([\x80-\xBF]{2})|\xED[\x80-\x9F][\x80-\xBF]|\xF0[\x90-\xBF]([\x80-\xBF]{2})|[\xF1-\xF3]([\x80-\xBF]{3})|\xF4[\x80-\x8F]([\x80-\xBF]{2})

%x substitution
%x if_cond
%s if_normal
%x regex
%x if_skip
%x variable
%s variable_extra

%%
	if (yyextra.skip_if) {
		yy_push_state(if_skip, yyscanner);
	}
	if (yyextra.end_sub) {
		yyextra.end_sub = false;
		yy_pop_state(yyscanner);
		return END_SUB;
	}

	/* option */
--{UTF_8_TEXT}+			{ yylval->s = strdup(yytext); return OPTION; }

<INITIAL,if_cond,if_normal,substitution>\$@\[-?[[:alnum:]]+\]	{ yylval->l = strtol(yytext + 3, NULL, 10); return ARGV_INDEX; }

	/*
	 * variable
	 * 	should match every conventional variable
	 */
<INITIAL,if_cond,if_normal,substitution>"$"	{ yy_push_state(variable, yyscanner); return START_VAR; }
<variable>[[:alnum:]_-]+			{ yylval->s = strdup(yytext); return VARIABLE_NAME; }
<variable>[/]					{ yy_push_state(variable_extra, yyscanner); yylval->s = strdup(yytext); return TEXT; }
<variable>[[:space:]]+				{ yy_pop_state(yyscanner); return END_VAR; }
<variable>`					{ yy_pop_state(yyscanner); yyextra.end_sub = true; return END_VAR; }
<variable_extra>[[:space:]]+			{ yy_pop_state(yyscanner); yy_pop_state(yyscanner); return END_VAR; }
<variable>.					{ fprintf(stderr, "illegal %s on line %i\n", yytext, yylineno); }
<variable_extra>{UTF_8_VAR}+			{ yylval->s = strdup(yytext); return TEXT; }
<variable_extra>`				{ yy_pop_state(yyscanner); yy_pop_state(yyscanner); yyextra.end_sub = true; return END_VAR; }

	/*
	 * command substitution
	 */
<INITIAL,if_cond,if_normal>`	{ yy_push_state(substitution, yyscanner); return START_SUB; }
<substitution>{UTF_8_SUBS}+	{ yylval->s = strdup(yytext); return SUB_TEXT; }
<substitution>`			{ yy_pop_state(yyscanner); return END_SUB; }
<substitution>.			/* ignore */
<substitution>\n		{ fprintf(stderr, "found linebreak in substitution on line %i\n", yylineno); exit(1); }


	/*
	 * C-like if statement
	 *
	 * we transition
	 *	INITIAL -> if_cond -> if_normal (match like outside)
	 *                    `-----> if_skip (match nothing if condition evaluates to false)
	 * inside 'if_cond' we may have other transitions, eg to support regex matching
	 */
"if"				{ yy_push_state(if_cond, yyscanner); }
	/* begin condition */
<if_cond>"("			{ return START_CONDITION; }
	/* command line of bwrap-exec */
<if_cond>$@			{ return ARGV; }
	/* perform regular expression match (left: string to match; right: regex) */
<if_cond>=~			{ return REG_MATCH; }

	/*
	 * regex
	 */
<if_cond>"/"			{ yy_push_state(regex, yyscanner); }
	/* if we encounter a single unescaped '/' we transition back to 'if_cond' */
<regex>"/"			{ yy_pop_state(yyscanner); }
	/* match all escaped characters to allow '/' in the regex */
<regex>\\{UTF_8_TEXT}		{ yylval->s = (yytext[1] == '/') ? strndup(yytext + 1, 1) : strdup(yytext); return ESCAPED_CHAR; }
	/* normal regex text */
<regex>[^\\/\n]+		{ yylval->s = strdup(yytext); return REGEX; }
<regex>\\|\n			{ fprintf(stderr, "illegal character (single backslash or newline) found in regex on line %i\n", yylineno); exit(1); }

	/* end condition */
<if_cond>")"			{ return END_CONDITION; }
<if_cond>[ \t\n]		/* ignore whitespace */
	/* TODO: make sure that we have returned previously to evaluate the condition*/
<if_cond>"{"			{ yy_push_state(if_normal, yyscanner); }
<if_cond>.			{ fprintf(stderr, "encountered unknown character '%s' while scanning the condition in line %i\n", yytext, yylineno); exit(1); }
	/* return back to normal */
<if_normal,if_skip>"}"		{ if (yy_top_state(yyscanner) == if_cond) { yy_pop_state(yyscanner); }; yy_pop_state(yyscanner); }

<if_skip>"{"			{ if (yyextra.skip_if) { yyextra.skip_if = false; } else { yy_push_state(if_skip, yyscanner); } }
<if_skip>[^{}]*|\n		/* ignore */

	/* text */
[[:alnum:]/_.][[:alnum:]/_.-]*	{ yylval->s = strdup(yytext); return TEXT; }

#.*\n				/* comment */
[[:space:]]+			/* ignore */

.				{ fprintf(stderr, "unrecognized token: '%s' on line %i, continuing anyway\n", yytext, yylineno); }
<<EOF>>				{ yyterminate(); }
