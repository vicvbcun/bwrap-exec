/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "utils.h"

void add_args(struct arguments *args, size_t n, ...) {
	va_list ap;
	va_start(ap, n);

	struct iovec *tmp = realloc(args->a, (args->n + n) * sizeof(struct iovec));
	if (!tmp) {
		exit(1);
	}

	args->a = tmp;

	for (size_t i = 0; i < n; i++) {
		char *s = va_arg(ap, char *);
		// strdup must have failed in the scanner
		if (!s) {
			va_end(ap);
			exit(1);
		}

		args->a[args->n + i].iov_base = s;
		// include the terminating '\0'
		args->a[args->n + i].iov_len = strlen(s) + 1;
	}

	args->n += n;

	va_end(ap);
}

char *read_env(char const *s)
{
	char *res = NULL;

	char *var = getenv(s);
	if (var) {
		res = strdup(var);
		if (!res) {
			exit(1);
		}

		return res;
	}

	bool allocated_home = false;
	// if 's' is one of the XDG Base Directory variables,
	// try to return the default value
	if (strcmp(s, XDG_CONFIG_HOME) == 0) {
		char *home = getenv("HOME");
		if (!home) {
			home = calloc(sizeof(char), 1);
			if (!home) {
				exit(1);
			}

			allocated_home = true;
		}
		res = calloc(sizeof(char), strlen(home) + strlen(XDG_CONFIG_SUFFIX) + 1);
		if (!res) {
			exit(1);
		}

		strcat(res, home);
		strcat(res, XDG_CONFIG_SUFFIX);

		if (allocated_home) {
			free(home);
		}

		return res;
	} else if (strcmp(s, XDG_CACHE_HOME) == 0) {
		char *home = getenv("HOME");
		if (!home) {
			home = calloc(sizeof(char), 1);
			if (!home) {
				exit(1);
			}

			allocated_home = true;
		}

		res = calloc(sizeof(char), strlen(home) + strlen(XDG_CACHE_SUFFIX) + 1);
		if (!res) {
			exit(1);
		}

		strcat(res, home);
		strcat(res, XDG_CACHE_SUFFIX);

		if (allocated_home) {
			free(home);
		}

		return res;
	} else if (strcmp(s, XDG_DATA_HOME) == 0) {
		char *home = getenv("HOME");
		if (!home) {
			home = calloc(sizeof(char), 1);
			if (!home) {
				exit(1);
			}

			allocated_home = true;
		}

		res = calloc(sizeof(char), strlen(home) + strlen(XDG_DATA_SUFFIX) + 1);
		if (!res) {
			exit(1);
		}

		// FIXME: maybe use sprintf?
		strcat(res, home);
		strcat(res, XDG_DATA_SUFFIX);

		if (allocated_home) {
			free(home);
		}
		
		return res;
	}

	// try to return an empty string
	res = calloc(sizeof(char), 1);
	if (!res) {
		exit(1);
	}

	return res;
}

char *read_command(struct array *a, int *exit_status)
{
	// the lexer has already tokenized the whole thing for us
	// the main thing we are concerned about here is reading the output

	int pipe_fd[2];
	if (pipe(pipe_fd)) {
		exit(1);
	}

	// we will store the output here
	char *result = NULL;
	size_t copied = 0;

	pid_t child = fork();
	if (child == 0) {
		// close read end
		close(pipe_fd[0]);

		// redirect stdout to pipe
		if (dup2(pipe_fd[1], STDOUT_FILENO) < 0) {
			exit(1);
		}

		char **tmp = realloc(a->s, (a->n + 1) * sizeof(char *));
		if (!tmp) {
			exit(1);
		}

		a->s = tmp;

		a->s[a->n] = NULL;

		execvp(a->s[0], a->s);

		exit(1);
	} else if (child > 0) {
		// close write end
		close(pipe_fd[1]);

		char buf[BUFLEN];
		ssize_t n = 0;
		size_t round = 0;
		while ( (n = read(pipe_fd[0], buf, sizeof(buf))) > 0) {
			//                                place for '\0'  --
			char *tmp = realloc(result, copied + BUFLEN + 1);
			if (!tmp) {
				exit(1);
			}

			result = tmp;

			// we have enough space allocated
			memcpy(result + copied, buf, n);

			copied += n;
			round++;
		}

		close(pipe_fd[0]);
	} else {
		// forking failed
		exit(1);
	}

	int w_status;
	waitpid(child, &w_status , 0);
	if (exit_status) {
		if (WIFEXITED(w_status)) {
			*exit_status = WEXITSTATUS(w_status);
		} else {
			// process exited abnormally
			*exit_status = -1;
		}
	}

	// we didn't read from 'cmd' for some reason,
	// try to return an empty string
	if (!result) {
		result = calloc(sizeof(char), 1);
		if (!result) {
			exit(1);
		}

		return result;
	}

	// ensure 'result' is NULL terminated and doesn't end with whitespace
	for (size_t i = copied; i > 0; i--) {
		// the last thing written is at index copied - 1
		// and we want to get to index 0
		if (isspace(result[i - 1])) {
			copied--;
		} else {
			break;
		}
	}

	result[copied] = '\0';

	// normalize whitespace to space
	for (size_t i = 0; i < copied; i++) {
		if (isspace(result[i]))
			result[i] = ' ';
	}

	return result;
}

int run_command(struct array *a)
{
	pid_t child = fork();
	if (child == 0) {
		int fd = open("/dev/null", O_WRONLY);
		if (fd < 0) {
			exit(1);
		}

		if (dup2(fd, STDOUT_FILENO) < 0) {
			exit(1);
		}

		if (dup2(fd, STDERR_FILENO) < 0) {
			exit(1);
		}

		char **tmp = realloc(a->s, (a->n + 1) * sizeof(char *));
		if (!tmp) {
			exit(1);
		}

		a->s = tmp;

		a->s[a->n] = NULL;

		execvp(a->s[0], a->s);

		exit(1);
	} else if (child > 0) {
		int w_status;
		waitpid(child, &w_status, 0);
		if (WIFEXITED(w_status)) {
			return WEXITSTATUS(w_status);
		} else {
			return -1;
		}
	} else {
		exit(1);
	}
}

void destroy_args(struct arguments args)
{
	for (size_t i = 0; i < args.n; i++) {
		free(args.a[i].iov_base);
	}

	free(args.a);
}
