/* SPDX-License-Identifier: GPL-3.0-or-later */

%define api.pure full
%define parse.error detailed

%define parse.trace

%code requires {
	/* this type is defined in the lex header, but this requires YSSTYPE */
	typedef void * yyscan_t;

	#include "common.h"
	#include "utils.h"
}

%{
	#include "parse.h"
	#include "scan.h"

	#include <sys/types.h>
	#include <sys/uio.h>
	#include <regex.h>

	void yyerror(yyscan_t, struct parse_args, char const *);
%}

%union {
	char *s;
	struct array *a;
	long l;
	bool truthness;
}

%lex-param {yyscan_t yyscanner}
%parse-param {yyscan_t yyscanner}
%parse-param {struct parse_args pargs}

%type <s>
	value
	substitution
	regex
	variable
	variable_val
	variable_extra

%type <a>
	sub_text

%type <truthness>
	reg_match

%token <l>
	ARGV_INDEX

%token <s>
	OPTION
	TEXT
	START_SUB
	END_SUB
	SUB_TEXT
	LINEFEED
	REG_MATCH
	ARGV
	REGEX
	ESCAPED_CHAR
	START_CONDITION
	END_CONDITION
	VARIABLE_NAME
	START_VAR
	END_VAR

%%

input:
	  %empty
	| input line
	| input START_CONDITION condition END_CONDITION
;

line:
	  OPTION		{ add_args(pargs.args, 1, $1); }
	| OPTION value		{ add_args(pargs.args, 2, $1, $2); }
	| OPTION value value	{ add_args(pargs.args, 3, $1, $2, $3); }
;

value:
	  TEXT			{ $$ = $1; }
	| variable		{ $$ = $1; }
	| substitution		{ $$ = $1; }
;

sub_text:
	  SUB_TEXT		{
		$$ = calloc(sizeof(struct array), 1);
		if (!$$) {
			exit(1);
		};

		$$->s = calloc(sizeof(char *), 1);
		if (!$$->s) {
			free($$);
			exit(1);
		};

		$$->n = 1; $$->s[0] = $1;
	}
	| variable		{
		$$ = calloc(sizeof(struct array), 1);
		if (!$$) {
			exit(1);
		};

		$$->s = calloc(sizeof(char *), 1);
		if (!$$->s) {
			free($$);
			exit(1);
		};

		$$->n = 1; $$->s[0] = $1;
	}
	| sub_text SUB_TEXT	{
		char **tmp = realloc($$->s, ($$->n + 1) * sizeof(char *));
		if (!tmp) {
			exit(1);
		}

		$$->s = tmp;
		$$->s[$$->n++] = $2;
	}
	| sub_text variable	{
		char **tmp = realloc($$->s, ($$->n + 1) * sizeof(char *));
		if (!tmp) {
			exit(1);
		}

		$$->s = tmp;
		$$->s[$$->n++] = $2;
	}
;

variable_extra:
	  TEXT			{ $$ = $1; }
	| variable_extra TEXT	{
		$$ = calloc(sizeof(char), strlen($1) + strlen($2) + 1);
		if (!$$) {
			exit(1);
		};

		sprintf($$, "%s%s", $1, $2);

		free($1), free($2);
	}
;

variable_val:
	  VARIABLE_NAME		{ $$ = read_env($1); free($1); }
	| VARIABLE_NAME variable_extra	{
		char *v = read_env($1);

		$$ = calloc(sizeof(char), strlen(v) + strlen($2) + 1);
		if (!$$) {
			exit(1);
		};

		sprintf($$, "%s%s", v, $2);

		free(v); free($1), free($2);
	}
;

variable:
	  START_VAR variable_val END_VAR	{ $$ = $2; }
	| ARGV_INDEX		{
		long index = $1;
		if (index >= 0 && index < pargs.argc) {
		} else if (index < 0 && -index <= pargs.argc) {
			index += pargs.argc;
		} else {
			fprintf(stderr, "index %li for argument list is out of bounds\n", index);
			exit(1);
		}

		$$ = strdup(pargs.argv[index]);
		if (!$$) {
			exit(1);
		}
	}
;

substitution:
	  START_SUB sub_text END_SUB	{
		$$ = read_command($2, NULL);
		// we don't need $2 anymore
		for (size_t i = 0; i < $2->n; i++) { free($2->s[i]); };
		free($2->s), free($2);
	}
;

condition:
	  reg_match {
		// indicate result to yylex
		if (!$1) {
			struct extra e = yyget_extra(yyscanner);
			e.skip_if = true;
			yyset_extra(e, yyscanner);
		}
	}
	| START_SUB sub_text END_SUB {
		if (run_command($2)) {
			struct extra e = yyget_extra(yyscanner);
			e.skip_if = true;
			yyset_extra(e, yyscanner);
		}

		// we don't need $2 anymore
		for (size_t i = 0; i < $2->n; i++) { free($2->s[i]); };
		free($2->s), free($2);
	}
;

reg_match:
	  ARGV REG_MATCH regex	{
		regex_t regex;
		int err = regcomp(&regex, $3, REG_NOSUB);
		if (err) {
			char buf[BUFLEN];
			size_t n = regerror(err, &regex, buf, BUFLEN);
			struct iovec vec[2];
			vec[0].iov_base = buf;
			vec[0].iov_len = n;
			vec[1].iov_base = "\n";
			vec[1].iov_len = 1;
			writev(STDOUT_FILENO, vec, 2);
			// maybe indicate skip instead of return?
			exit(1);
		}

		int i = 0;
		$$ = false;
		while (pargs.argv[i]) {
			if (regexec(&regex, pargs.argv[i], 0, NULL, 0) == 0) {
				$$ = true;
				break;
			}

			i++;
		}

		regfree(&regex);
		free($3);
	}
	| value REG_MATCH regex	{
		regex_t regex;
		int err = regcomp(&regex, $3, REG_NOSUB);
		if (err) {
			char buf[BUFLEN];
			size_t n = regerror(err, &regex, buf, BUFLEN);
			struct iovec vec[2];
			vec[0].iov_base = buf;
			vec[0].iov_len = n;
			vec[1].iov_base = "\n";
			vec[1].iov_len = 1;
			writev(STDOUT_FILENO, vec, 2);
			// maybe indicate skip instead of return?
			exit(1);
		}

		$$ = false;
		if (regexec(&regex, $1, 0, NULL, 0) == 0) {
			$$ = true;
		}

		regfree(&regex);
		free($1);
		free($3);
	}
;

regex:
	  %empty		{ $$ = calloc(sizeof(char), 1); if (!$$) { exit(1); }; }
	| regex REGEX		{ $$ = calloc(sizeof(char), strlen($1) + strlen($2) + 1); if (!$$) { exit(1); }; sprintf($$, "%s%s", $1, $2); free($1); free($2); }
	| regex ESCAPED_CHAR	{ $$ = calloc(sizeof(char), strlen($1) + strlen($2) + 1); if (!$$) { exit(1); }; sprintf($$, "%s%s", $1, $2); free($1); free($2); }
;

%%

void yyerror(yyscan_t p, struct parse_args pargs, char const *s)
{
	(void)p, (void)pargs;
	fprintf (stderr, "%s\n", s);
}
