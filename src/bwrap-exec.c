/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "bwrap-exec.h"

struct options options = {
	.keep_env = false,
	.policy_dir = NULL,
	.command = NULL,
	.normalize_args = false
};

noreturn static void usage(int err, char * const argv[])
{
	char string[] =
		"Usage:\n"
		"\t%s [options] <command> [more arguments]\n"
		"\n"
		"Options:\n"
		"  -h\t\tdisplay this help message\n"
		"  -k\t\tdon't clear the environment\n"
		"  -p <path>\tlook for policies here\n"
		"  -c <command>\tuse this as command\n"
		"  -n\ttry to normalize pathnames in arguments\n"
		"  -V\t\tdisplay version info\n"
	;

	fprintf(err ? stderr : stdout, string, argv[0]);

	exit(err);
}

noreturn static void license(void)
{
	char string[] =
		"bwrap-exec %s\n"
		"Copyright (C) 2020 vicvbcun\n"
		"\n"
		"This program is free software: you can redistribute it and/or modify\n"
		"it under the terms of the GNU General Public License as published by\n"
		"the Free Software Foundation, either version 3 of the License, or\n"
		"(at your option) any later version.\n"
                "\n"
		"This program is distributed in the hope that it will be useful,\n"
		"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
		"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
		"GNU General Public License for more details.\n"
                "\n"
		"You should have received a copy of the GNU General Public License\n"
		"along with this program.  If not, see <https://www.gnu.org/licenses/>.\n"
	;

	printf(string, BWRAP_EXEC_VERSION);

	exit(EXIT_SUCCESS);
}

/*
 * specify command seperately to allow trickery
 * e.g. not using the name on the command-line but another one
 */
static char **create_argv(int argc, char * const argv[], int offset, char *cmd, int fd)
{
	/*
	 * 4:			static arguments to bwrap
	 * 1:			cmd
	 * argc - offset - 1:	user arguments passed to bwrap (excluding command)
	 * 1:			NULL
	 * and:			argc > offset
	 */
	int n_argc = argc + 5 - offset;

	// now it's the index of the first argument to copy
	offset++;

	char **n_argv = calloc(sizeof(char *), n_argc);
	if (!n_argv) {
		return NULL;
	}

	// every int should fit into 10 chars as fd > 0
	// one additional char for '\0'
	char *fd_string = calloc(sizeof(char), 11);
	if (!fd_string) {
		free(n_argv);
		return NULL;
	}

	snprintf(fd_string, 11, "%i", fd);

	n_argv[0] = BWRAP_CMD;
	n_argv[1] = "--args";
	n_argv[2] = fd_string;
	n_argv[3] = "--";

	n_argv[4] = cmd;

	int cleanup_till;
	// start at index 5
	// the last should be NULL so we can ignore it
	for (int i = 5; i < n_argc - 1; i++) {
		// undo shift                --
		int cur = i + offset - 5;
		if (options.normalize_args) {
			char *tmp = realpath(argv[cur], NULL);
			if (!tmp) {
				tmp = strdup(argv[cur]);
				if (!tmp) {
					// we need to free everything
					cleanup_till = i;
					goto cleanup;
				}
			}

			n_argv[i] = tmp;
		} else {
			n_argv[i] = argv[cur];
		}
	}

	// terminate
	n_argv[n_argc - 1] = NULL;

	return n_argv;

cleanup:
	free(fd_string);

	// start allocated stuff starts at index 5
	for (int i = 5; i < cleanup_till; i++) {
		free(n_argv[i]);
	}

	free(n_argv);

	return NULL;
}

static int parse_args(int argc, char * const argv[])
{
	// prevent glibc from doing stupid things
	// should be unnecessary
	bool posix_was_set = !!getenv("POSIXLY_CORRECT");
	if (!posix_was_set) {
		if (setenv("POSIXLY_CORRECT", "1", 0)) {
			perror("setenv");
			exit(EXIT_FAILURE);
		}
	}

	int opt;
	while ( (opt = getopt(argc, argv, "hkp:c:nV")) != -1 ) {
		switch (opt) {
		case 'k':
			options.keep_env = true;
			break;
		case 'p':
			options.policy_dir = optarg;
			break;
		case 'c':
			options.command = optarg;
			break;
		case 'h':
			usage(EXIT_SUCCESS, argv);
		case 'n':
			options.normalize_args = true;
			break;
		case 'V':
			license();
		default: // unknown option
			usage(EXIT_FAILURE, argv);
		}
	}

	// no non-option argument provided
	// or no option at all
	if (optind == argc) {
		usage(EXIT_FAILURE, argv);
	}

	if (!posix_was_set) {
		unsetenv("POSIXLY_CORRECT");
	}

	return optind;
}

static FILE *open_policy_file(const char *name)
{
	const char *search_dirs[] = {
		options.policy_dir,
		getenv("BWRAP_EXEC_SEARCH_DIR"),
		DEFAULT_SEARCH_DIR
	};

	size_t len = 0;
	char *policy_file = NULL;

	size_t num = sizeof(search_dirs) / sizeof(search_dirs[0]);

	// try each location
	for (size_t i = 0; i < num; i++) {
		if (!search_dirs[i]) {
			continue;
		}

		// one for '\' and one for '\0'
		len = strlen(search_dirs[i]) + strlen(name) + 2;
		char *tmp = realloc(policy_file, sizeof(char) * len);
		if (!tmp) {
			perror("realloc");
			return NULL;
		}

		policy_file = tmp;

		snprintf(policy_file, len, "%s/%s", search_dirs[i], name);

		FILE *f = fopen(policy_file, "r");
		if (f) {
			free(policy_file);
			return f;
		}
	}

	free(policy_file);

	fprintf(stderr, "no policy file for \"%s\" found\n", name);

	return NULL;
}

int main(int argc, char *argv[])
{
	int offset = parse_args(argc, argv);

	char *command = (options.command) ? options.command : argv[offset];

	yyscan_t scanner;
	struct extra e = {
		.skip_if = false,
		.end_sub = false
	};
	yylex_init_extra(e, &scanner);

	FILE *f = open_policy_file(argv[offset]);
	if (!f) {
		yylex_destroy(scanner);
		exit(EXIT_FAILURE);
	}

	yyset_in(f, scanner);
	struct arguments arg_list = {
		.n = 0,
		.a = NULL
	};

	yydebug = 0;
	yyset_debug(0, scanner);

	struct parse_args pargs = {
		.args = &arg_list,
		.argv = argv + offset,
		.argc = argc - offset
	};

	int err = yyparse(scanner, pargs);
	if (err) {
		printf("parsing error, aborting\n");
		exit(1);
	}

	yylex_destroy(scanner);
	fclose(f);

	size_t num = arg_list.n;
	struct iovec *vector = arg_list.a;

	// get pipe
	int pipe_fd[2];
	if (pipe(pipe_fd)) {
		perror("pipe");
		destroy_args(arg_list);
		exit(EXIT_FAILURE);
	}

	// write from child into pipe so that the writing process
	// can exit without reparenting
	pid_t child = fork();
	if (child == 0) {
		// close read end
		close(pipe_fd[0]);

		ssize_t err;
		while (true) {
			if (num <= IOV_MAX) {
				err = writev(pipe_fd[1], vector, num);
				break;
			} else {
				err = writev(pipe_fd[1], vector, IOV_MAX);
				if (err < 0) {
					break;
				}

				vector += IOV_MAX;
				num -= IOV_MAX;
			}
		}

		if (err < 0) {
			perror("writev");
		}

		close(pipe_fd[1]);

		// deallocates everthing accessible through vector
		destroy_args(arg_list);

		exit(EXIT_SUCCESS);
	} else if (child > 0) {
		// close write end
		close(pipe_fd[1]);

		char **n_argv = create_argv(argc, argv, offset, command, pipe_fd[0]);
		if (!n_argv) {
			perror("calloc");
			exit(1);
		}

		char *env[] = { NULL };
		char **envp = options.keep_env ? environ : env;
		execve(BWRAP_CMD, n_argv, envp);

		// execve failed
		perror("exec");

		// deallocates everthing accessible through vector
		destroy_args(arg_list);

		// fd_string
		free(n_argv[2]);

		// allocated stuf
		if (options.normalize_args) {
			int i = 5;
			while (n_argv[i]) {
				free(n_argv[i++]);
			}
		}

		free(n_argv);

		exit(EXIT_FAILURE);
	}

	// forking failed
	perror("fork");

	// deallocates everthing accessible through vector
	destroy_args(arg_list);

	return EXIT_FAILURE;
}
