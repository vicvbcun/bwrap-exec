bwrap-exec
==========

Too many options for `bubblewrap`? Fear no more!
Pass options to `bubblewrap` by writing them into a file.

Requirements
------------
* flex
* bison

Syntax
------
Passing options is as simple as writing them into a policy file verbatim:

    --dev /dev
    --ro-bind-try /usr/bin/firefox /usr/bin/firefox

`bwrap-exec` supports variable expansion (including expanding `XDG_*_HOME` to their default values) and simple command substitution,
so you can write:

    --setenv HOME $HOME
    --bind-try $HOME $HOME
    --bind-try `pwd` `pwd`

When you know the command-line arguments, you can also refer to a specific argument by its index:

    --ro-bind-try $@[2] $@[2]

See commit `b6aab919` for more information.

You can also make certain parts of a policy optional by writing if statements

    if (some-condition) {
        ...
    }

where `some-condition` is (for now) a regex (POSIX Basic Regular expression) match, matching against a variable,
the result of a command substitution, fixed text (but why would you) and the special 'variable' `$@` meaning all command-line arguments:

    if ($HOME =~ /some-username/) {
        ...

        if ($@ =~ /^-localhost$/) {
            --unshare-net
        }

        ...
    }

Comments start with `#`.

Compiling and Installing
------------------------
Just run `make` and/or `make install`.
You can put policy files into `/etc/bwrap-exec`.
Notes:
- when compiling with `clang` comment out the definition of `CFLAGS_DEBUG` from the Makefile
- when using `bison < 3.7` remove `-Wcex` from `YFLAGS`

Security
--------
`bwrap-exec` just passes the options specified inside a policy to `bubblewrap` using its `--args FD` flag.
Therefore most security relevant things outsourced.  
**However**: As `bwrap-exec` supports command substitution, you should only use trusted policies.
At the moment, it is possible to control the location which `bwrap-exec` will search for policies.

Limitations
-----------
Currently, there is no way to have spaces inside e.g. filenames.
The scanner is a mess and tries to match UTF-8 encoded characters.
`bubblewrap` allows up to 9000 arguments. `bwrap-exec` doesn't impose any limit itself, but always passes 5 arguments itself.
Thus there is an effective limit of 8995 arguments.
