/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef COMMON_H
#define COMMON_H

#include <stddef.h>

#include <sys/uio.h>

struct array {
	size_t n;
	char **s;
};

struct arguments {
	size_t n;
	struct iovec *a;
};

struct parse_args {
	struct arguments *args;
	char **argv;
	long argc;
};

#endif
