/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef BWRAP_EXEC_H
#define BWRAP_EXEC_H

#define _XOPEN_SOURCE 700

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdnoreturn.h>

#include <string.h>

#include <unistd.h>

#include <sys/types.h>
#include <sys/uio.h>

#include <limits.h>

#include "common.h"
#include "parse.h"
#include "scan.h"

#define BWRAP_EXEC_VERSION	"0.1"

#define BWRAP_CMD		"/usr/bin/bwrap"

#define DEFAULT_SEARCH_DIR	"/etc/bwrap-exec"

extern char **environ;

struct options {
	bool keep_env;
	const char *policy_dir;
	char *command;
	bool normalize_args;
};


#endif
