/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>

#include <ctype.h>

#include <unistd.h>

#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>

#include "common.h"

#define XDG_CONFIG_HOME "XDG_CONFIG_HOME"
#define XDG_CACHE_HOME "XDG_CACHE_HOME"
#define XDG_DATA_HOME "XDG_DATA_HOME"
#define XDG_CONFIG_SUFFIX "/.config"
#define XDG_CACHE_SUFFIX "/.cache"
#define XDG_DATA_SUFFIX "/.local/share"

#define BUFLEN	256

/*
 * note:
 * 	- may not return
 */
void add_args(struct arguments *, size_t, ...);

/*
 * note:
 * 	- may not return
 *
 * return:
 * 	- allocated 'char *' to the expansion of argument
 * 	- NULL
 */
char *read_env(char const *);

/*
 * note:
 * 	- may not return
 *
 * return:
 * 	- allocated 'char *' to the output of argument
 * 	- NULL
 */
char *read_command(struct array *, int *);

/*
 * note:
 * 	- may not return
 *
 * return:
 * 	- exit status of the command
 * 	- -1
 */
int run_command(struct array *);

/*
 * free all memory referenced by argument
 */
void destroy_args(struct arguments);

#endif
